package org.nsu.fit.tm_backend.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.nsu.fit.tm_backend.database.data.ContactPojo;
import org.nsu.fit.tm_backend.database.data.TopUpBalancePojo;
import org.nsu.fit.tm_backend.manager.auth.data.AuthenticatedUserDetails;
import org.nsu.fit.tm_backend.shared.Authority;
import org.nsu.fit.tm_backend.shared.Globals;
import org.slf4j.Logger;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

// Лабораторная 2: покрыть юнит тестами класс CustomerManager на 100%.
class CustomerManagerTest {
    private Logger logger;
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;
    private List<CustomerPojo> customerPojos;

    @BeforeEach
    void init() {
        // Создаем mock объекты.
        dbService = mock(IDBService.class);
        logger = mock(Logger.class);

        // Создаем класс, методы которого будем тестировать,
        // и передаем ему наши mock объекты.
        customerManager = new CustomerManager(dbService, logger);

        // настраиваем mock.
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;
    }

    void initCustomerListMock() {
        customerPojos = new ArrayList<>();
        customerPojos.add(createCustomerInput);
        when(dbService.getCustomers()).thenReturn(customerPojos);
        when(dbService.getCustomer(createCustomerInput.id)).thenReturn(createCustomerInput);
        doAnswer(i -> customerPojos.remove(createCustomerInput)).when(dbService).deleteCustomer(createCustomerInput.id);
        when(dbService.getCustomerByLogin(createCustomerInput.login)).thenReturn(createCustomerInput);
        doAnswer(i -> createCustomerInput = i.getArgument(0)).when(dbService).editCustomer(createCustomerInput);
    }

    @Test
    void testCreateCustomer() {
        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@example.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    @Test
    void testCreateCustomerWithNullArgument() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(null));
        assertEquals("Argument 'customer' is null.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithNullPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = null;
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Field 'customer.pass' is null.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithShortPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "123";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithLongPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "1231231231234";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password's length should be more or equal 6 symbols and less or equal 12 symbols.", exception.getMessage());
    }

    @Test
    void testCreateCustomerWithVeryEasyPassword() {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "123qwe";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is very easy.", exception.getMessage());

        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "1q2w3e";
        createCustomerInput.balance = 0;

        exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Password is very easy.", exception.getMessage());
    }



    @Test
    void testCreateCustomerWithRepeatedLogin() {
        initCustomerListMock();
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@example.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(createCustomerInput));
        assertEquals("Customer with this login already exist", exception.getMessage());
    }

    @Test
    void testGetCustomers() {
        initCustomerListMock();

        List<CustomerPojo> result = customerManager.getCustomers();

        assertEquals(customerPojos, result);

        verify(dbService, times(1)).getCustomers();
    }

    @Test
    void testGetCustomer() {
        initCustomerListMock();

        CustomerPojo result = customerManager.getCustomer(createCustomerInput.id);

        assertEquals(createCustomerInput, result);

        verify(dbService, times(1)).getCustomer(createCustomerInput.id);
    }

    @Test
    void testLookupCustomer() {
        initCustomerListMock();

        CustomerPojo result = customerManager.lookupCustomer(createCustomerInput.login);

        assertEquals(createCustomerInput, result);

        verify(dbService, times(1)).getCustomers();
    }

    @Test
    void testLookupCustomerWithNullArgument() {
        initCustomerListMock();

        CustomerPojo result = customerManager.lookupCustomer(null);

        assertNull(result);

        verify(dbService, times(1)).getCustomers();
    }

    @Test
    void testLookupCustomerWithNotPresentLogin() {
        initCustomerListMock();

        CustomerPojo result = customerManager.lookupCustomer("not present");

        assertNull(result);

        verify(dbService, times(1)).getCustomers();
    }

    @Test
    void testMeAdmin() {
        AuthenticatedUserDetails adminDetails = new AuthenticatedUserDetails(null,
                createCustomerInput.login, new HashSet<>(Collections.singletonList(Authority.ADMIN_ROLE)));

        ContactPojo result = customerManager.me(adminDetails);

        assertEquals(result.login, Globals.ADMIN_LOGIN);
        assertNull(result.firstName);
        assertNull(result.lastName);
        assertEquals(result.balance, 0);
        assertNull(result.pass);

        verify(dbService, times(0)).getCustomerByLogin(createCustomerInput.login);
    }

    @Test
    void testMeNotAdmin() {
        initCustomerListMock();

        AuthenticatedUserDetails details = new AuthenticatedUserDetails(null,
                createCustomerInput.login, new HashSet<>());

        ContactPojo result = customerManager.me(details);

        assertEquals(result, createCustomerInput);
        assertEquals(result.getClass(), CustomerPojo.class);

        verify(dbService, times(1)).getCustomerByLogin(createCustomerInput.login);
    }

    @Test
    void testDeleteCustomer() {
        initCustomerListMock();

        customerManager.deleteCustomer(createCustomerInput.id);

        assertFalse(dbService.getCustomers().contains(createCustomerInput));

        verify(dbService, times(1)).deleteCustomer(createCustomerInput.id);
    }

    @Test
    void testTopUpBalanceZero() {
        initCustomerListMock();

        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.topUpBalance(new TopUpBalancePojo()));
        assertEquals("Money must be more than 0", exception.getMessage());
    }

    @Test
    void testTopUpBalance() {
        initCustomerListMock();

        TopUpBalancePojo topUpBalancePojo = new TopUpBalancePojo();
        topUpBalancePojo.money = 10;

        customerManager.topUpBalance(topUpBalancePojo);

        assertEquals(createCustomerInput.balance, 10);
    }
}
