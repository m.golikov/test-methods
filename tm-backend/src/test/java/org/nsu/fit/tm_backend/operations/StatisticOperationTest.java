package org.nsu.fit.tm_backend.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.SubscriptionPojo;
import org.nsu.fit.tm_backend.manager.CustomerManager;
import org.nsu.fit.tm_backend.manager.SubscriptionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class StatisticOperationTest {
    // Лабораторная 2: покрыть юнит тестами класс StatisticOperation на 100%.

    private CustomerManager customerManager;
    private SubscriptionManager subscriptionManager;
    private List<UUID> customerIds;

    private StatisticOperation statisticOperation;

    @BeforeEach
    void init() {
        customerManager = mock(CustomerManager.class);
        subscriptionManager = mock(SubscriptionManager.class);

        customerIds = new ArrayList<UUID>();
        customerIds.add(UUID.randomUUID());
        customerIds.add(UUID.randomUUID());

        statisticOperation = new StatisticOperation(customerManager,subscriptionManager,customerIds);
    }

    @Test
    void testCustomerManagerIsNull() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                new StatisticOperation(null,subscriptionManager,customerIds));
        assertEquals("customerManager", exception.getMessage());
    }

    @Test
    void testSubscriptionManagerIsNull() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                new StatisticOperation(customerManager,null,customerIds));
        assertEquals("subscriptionManager", exception.getMessage());
    }

    @Test
    void testCustomersIdsIsNull() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                new StatisticOperation(customerManager,subscriptionManager,null));
        assertEquals("customerIds", exception.getMessage());
    }

    @Test
    void testExecute() {
        StatisticOperation.StatisticOperationResult statisticOperationResult = new StatisticOperation.StatisticOperationResult();
        statisticOperationResult.overallBalance = 10;
        statisticOperationResult.overallFee = 20;

        for (UUID uuid : customerIds) {
            CustomerPojo customer = new CustomerPojo();
            customer.balance = 5;
            customer.id = uuid;

            List<SubscriptionPojo> subscriptions = new ArrayList<>();
            SubscriptionPojo subscription = new SubscriptionPojo();
            subscription.planFee = 10;
            subscriptions.add(subscription);

            when(customerManager.getCustomer(uuid)).thenReturn(customer);
            when(subscriptionManager.getSubscriptions(uuid)).thenReturn(subscriptions);
        }

        StatisticOperation.StatisticOperationResult result = statisticOperation.Execute();

        assertEquals(result.overallBalance, statisticOperationResult.overallBalance);
        assertEquals(result.overallFee, statisticOperationResult.overallFee);

        verify(customerManager, times(2)).getCustomer(any());
        verify(subscriptionManager, times(2)).getSubscriptions(any());

    }

}
