package org.nsu.fit.services.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.glassfish.jersey.client.ClientConfig;
import org.nsu.fit.services.log.Logger;
import org.nsu.fit.services.rest.data.*;
import org.nsu.fit.shared.JsonMapper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RestClient {
    // Note: change url if you want to use the docker compose.
//    private static final String REST_URI = "http://localhost:8080/tm-backend/rest";
    private static final String REST_URI = "http://localhost:8089/tm-backend/rest";

    private final static Client client = ClientBuilder.newClient(new ClientConfig().register(RestClientLogFilter.class));

    public AccountTokenPojo authenticate(String login, String pass) {
        CredentialsPojo credentialsPojo = new CredentialsPojo();

        credentialsPojo.login = login;
        credentialsPojo.pass = pass;

        return post("authenticate", JsonMapper.toJson(credentialsPojo, true), AccountTokenPojo.class, null);
    }

    public CustomerPojo createCustomer(AccountTokenPojo accountToken, ContactPojo contactPojo) {

        // Лабораторная 3: Добавить обработку генерацию фейковых имен, фамилий и логинов.
        // * Исследовать этот вопрос более детально, возможно прикрутить специальную библиотеку для генерации фейковых данных.

        return post("customers", JsonMapper.toJson(contactPojo, true), CustomerPojo.class, accountToken);
    }

    public PlanPojo createPlan(AccountTokenPojo accountTokenPojo, PlanPojo planPojo) {
        return post("plans", JsonMapper.toJson(planPojo, true), PlanPojo.class, accountTokenPojo);
    }

    public SubscriptionPojo createSubscription (AccountTokenPojo accountTokenPojo, SubscriptionPojo subscriptionPojo) {
        return post("subscriptions", JsonMapper.toJson(subscriptionPojo,true), SubscriptionPojo.class, accountTokenPojo);
    }

    private static <R> R post(String path, String body, Class<R> responseType, AccountTokenPojo accountToken) {
        // Лабораторная 3: Добавить обработку Responses и Errors. Выводите их в лог.
        // Подумайте почему в filter нет Response чтобы можно было удобно его сохранить.

        return request(path, responseType,accountToken, request -> request.post(Entity.entity(body, MediaType.APPLICATION_JSON), String.class) );
    }

    private static <R> R delete(String path, Class<R> responseType, AccountTokenPojo accountToken){

        return request(path,responseType,accountToken, request -> request.delete(String.class));
    }

    public void deleteCustomer (AccountTokenPojo accountTokenPojo, String path) {
        delete("customers/" + path, void.class, accountTokenPojo);
    }

    public void deletePlan (AccountTokenPojo accountTokenPojo, String path) {
        delete ("plans/" + path, void.class, accountTokenPojo);
    }

    public void deleteSubscription(AccountTokenPojo accountTokenPojo, String path) {
        delete("subscriptions/" + path, void.class, accountTokenPojo);
    }

    private static <R> R get(String path, Class<R> responseType, AccountTokenPojo accountToken){
        return request(path,responseType,accountToken, request -> request.get(String.class));
    }

    public CustomerPojo getMe(AccountTokenPojo accountTokenPojo) {
        return get("me", CustomerPojo.class, accountTokenPojo);
    }

    private static class CustomerList extends ArrayList<CustomerPojo> {}
    public List<CustomerPojo> getCustomers (AccountTokenPojo accountTokenPojo) {
        return get("customers", CustomerList.class, accountTokenPojo);
    }

    private static class PlanList extends ArrayList<PlanPojo> {}
    public List<PlanPojo> getPlans (AccountTokenPojo accountTokenPojo) {
        return get("plans", PlanList.class, accountTokenPojo);
    }

    public List<PlanPojo> getAvailablePlans (AccountTokenPojo accountTokenPojo) {
        return  get("available_plans", PlanList.class, accountTokenPojo);
    }

    public List<String> getSubscriptions (AccountTokenPojo accountTokenPojo, String customerId) {
        return get("subscriptions", List.class, accountTokenPojo);
    }

    private static Invocation.Builder createRequest (String path, AccountTokenPojo accountToken){
        Invocation.Builder request = client
                .target(REST_URI)
                .path(path)
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        if (accountToken != null) {
            request.header("Authorization", "Bearer " + accountToken.token);
        }

        return request;
    }

    private static <R> R parseResponse(String response, Class<R> responseType){
        try {
            R r = Optional.ofNullable(response)
                    .filter(str->!str.isEmpty())
                    .map(res -> JsonMapper.fromJson(response, responseType))
                    .orElse(null);
            Logger.debug(response);
            return r;
        }
        catch (RuntimeException re) {
            Logger.error(re.getMessage(), re);
            throw re;
        }
    }

    private static <R> R request(String path, Class<R> responseType, AccountTokenPojo accountToken, Function<Invocation.Builder, String> method){
        Invocation.Builder request = createRequest(path, accountToken);

        String response = method.apply(request);

        return parseResponse(response, responseType);
    }

    public CustomerPojo generateCustomer (){
        CustomerPojo customerPojo = new CustomerPojo();

        Faker faker = new Faker();

        customerPojo.firstName = faker.name().firstName();
        customerPojo.lastName = faker.name().lastName();
        customerPojo.login = faker.internet().emailAddress();
        customerPojo.pass = faker.internet().password(6,11);

        return customerPojo;
    }

    public PlanPojo generatePlan () {
        PlanPojo planPojo = new PlanPojo();

        Faker faker = new Faker();

        planPojo.details = faker.commerce().productName();
        planPojo.name = faker.name().name();
        planPojo.fee = faker.number().numberBetween(1, 10);

        return planPojo;
    }

    public SubscriptionPojo generateSubscription(PlanPojo planPojo) {
        SubscriptionPojo subscriptionPojo = new SubscriptionPojo();

        subscriptionPojo.planId = planPojo.id;
        subscriptionPojo.planName = planPojo.name;
        subscriptionPojo.planDetails = planPojo.details;
        subscriptionPojo.planFee = planPojo.fee;

        return subscriptionPojo;
    }

    private static class RestClientLogFilter implements ClientRequestFilter {
        @Override
        public void filter(ClientRequestContext requestContext) {
            if(requestContext.hasEntity()) Logger.debug(requestContext.getEntity().toString());
            Logger.debug(requestContext.getMethod());
            Logger.debug(requestContext.getHeaders()
                    .entrySet()
                    .stream()
                    .map(e ->
                            e.getKey() + ": " +
                                    e.getValue()
                                            .stream()
                                            .map(Object::toString)
                                            .collect(Collectors.joining(", ")))
                    .collect(Collectors.joining("\n")));

            // Лабораторная 3: разобраться как работает данный фильтр
            // и добавить логирование METHOD и HEADERS.
        }
    }
}
