package org.nsu.fit.tests.ui.Customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.services.browser.BrowserService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreateCustomerTest {
    private Browser browser = null;
    private RestClient restClient;
    private AccountTokenPojo adminToken;
    private CustomerPojo customerPojo;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
        browser = BrowserService.openNewBrowser();
        customerPojo = restClient.generateCustomer();
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Create customer via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Customer feature")
    public void createCustomer() {
        new LoginScreen(browser)
                .loginAsAdmin()
                .createCustomer()
                .fillEmail(customerPojo.login)
                .fillPassword(customerPojo.pass)
                .fillFirstName(customerPojo.firstName)
                .fillLastName(customerPojo.lastName)
                .clickSubmit();


        List<CustomerPojo> customers = restClient.getCustomers(adminToken);
        assertNotNull(customers);
        assertNotEquals(customers.size(), 0);

        List<CustomerPojo> customer = customers.stream()
                .filter(c -> Objects.equals(c.login, customerPojo.login))
                .collect(Collectors.toList());
        assertNotNull(customer);
        assertEquals(customer.size(), 1);
        assertEquals(customerPojo.login, customer.get(0).login);
        assertEquals(customerPojo.login, customer.get(0).pass);
        assertEquals(customerPojo.login, customer.get(0).login);
        assertEquals(customerPojo.login, customer.get(0).login);
        customerPojo = customer.get(0);

        // Лабораторная 4: Проверить что customer создан с ранее переданными полями.
        // Решить проблему с генерацией случайных данных.
    }

    @AfterClass
    public void afterClass() {
        restClient.deleteCustomer(adminToken, customerPojo.id.toString());
        if (browser != null) {
            browser.close();
        }
    }
}
