package org.nsu.fit.tests.ui.Auth;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.services.browser.BrowserService;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AuthAsCustomerTest {

    private Browser browser = null;
    private RestClient restClient;
    private AccountTokenPojo adminToken;
    private CustomerPojo customerPojo;

    @BeforeClass
    public void beforeClass () {
        browser = BrowserService.openNewBrowser();
        restClient = new RestClient();
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Authenticate as customer via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Authenticate feature.")
    public void authAsAdmin () {
        customerPojo = restClient.createCustomer(adminToken,restClient.generateCustomer());
        new LoginScreen(browser).loginAsCustomer(customerPojo.login,customerPojo.pass);
    }

    @AfterClass
    public void afterClass() {
        restClient.deleteCustomer(adminToken,customerPojo.id.toString());
        if (browser != null) {
            browser.close();
        }
    }

}
