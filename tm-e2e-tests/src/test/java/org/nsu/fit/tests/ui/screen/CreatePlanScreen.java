package org.nsu.fit.tests.ui.screen;

import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.shared.Screen;
import org.openqa.selenium.By;

public class CreatePlanScreen extends Screen {
    public CreatePlanScreen(Browser browser) {
        super(browser);
    }

    public CreatePlanScreen fillName(String name) {
        browser.typeText(By.name("name"), name);
        return this;
    }

    public CreatePlanScreen fillDetails(String details) {
        browser.typeText(By.name("details"), details);
        return this;
    }

    public CreatePlanScreen fillFee(Integer fee) {
        browser.typeText(By.name("fee"),fee.toString());
        return this;
    }

    public AdminScreen clickSubmit() {
        browser.click(By.xpath("//button[@type = 'submit']"));
        By a = By.xpath("//div[@role = 'alert']");
        if(browser.isElementPresent(a)) {
            throw new RuntimeException(browser.getText(a));
        }
        return new AdminScreen(browser);
    }

    public AdminScreen clickCancel() {
        browser.click(By.xpath("//button[@type = 'button']"));
        return new AdminScreen(browser);
    }
}
