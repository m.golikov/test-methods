package org.nsu.fit.tests.ui.screen;

import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.shared.Screen;
import org.openqa.selenium.By;

public class AdminScreen extends Screen {
    public AdminScreen(Browser browser) {
        super(browser);
    }

    public CreateCustomerScreen createCustomer() {
        By button = By.xpath("//button[@title = 'Add Customer']");
        browser.waitForElement(button);
        browser.click(button);
        return new CreateCustomerScreen(browser);
    }

    public CreatePlanScreen createPlan () {
        By button = By.xpath("//button[@title = 'Add plan']");
        browser.waitForElement(button);
        browser.click(button);
        return new CreatePlanScreen(browser);
    }

    public LoginScreen logout () {
        By button = By.id("logout");
        browser.waitForElement(button);
        browser.click(button);
        return new LoginScreen(browser);
    }

    public AdminScreen deleteCustomer (String login) {
        By button = By.xpath("//div[div/div/h6[contains(text(), 'Customers')]]//tr[td[contains(text(), '" + login + "')]]//button[@title = 'Delete']");
        By button2 = By.xpath("//button[@title = 'Save']");
        browser.waitForElement(button);
        browser.click(button);
        browser.waitForElement(button2);
        browser.click(button2);
        return this;
    }

    public AdminScreen searchForCustomer(String login) {
        By search = By.xpath("//div[div/h6[contains(text(), 'Customers')]]//input");
        browser.typeText(search, login);
        return this;
    }

    public AdminScreen deletePlan (String login) {
        By button = By.xpath("//div[div/div/h6[contains(text(), 'Plans')]]//tr[td[contains(text(), '" + login + "')]]//button[@title = 'Delete']");
        By button2 = By.xpath("//button[@title = 'Save']");
        browser.waitForElement(button);
        browser.click(button);
        browser.waitForElement(button2);
        browser.click(button2);
        return this;
    }

    public AdminScreen searchForPlan(String login) {
        By search = By.xpath("//div[div/h6[contains(text(), 'Plans')]]//input");
        browser.typeText(search, login);
        return this;
    }
}
