package org.nsu.fit.tests.api.Plan;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreatePlanTest {

    private AccountTokenPojo adminToken;
    private PlanPojo plan;

    private RestClient restClient;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Create plan")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Plan feature.")
    public void createPlanTest() {
        PlanPojo planPojo = restClient.generatePlan();
        plan = restClient.createPlan(adminToken, planPojo);

        assertNotNull(plan);
        assertEquals(planPojo.details, plan.details);
        assertEquals(planPojo.name, plan.name);
        assertEquals(planPojo.fee, plan.fee);
    }

    @AfterClass
    public void afterClass() {
        restClient.deletePlan(adminToken, plan.id.toString());
    }
}
