package org.nsu.fit.tests.api.Customer;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DeleteCustomerTest {

    private AccountTokenPojo adminToken;
    private CustomerPojo customerToken;

    private RestClient restClient;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Create customer")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Customer feature.")
    public void createCustomerTest() {
        CustomerPojo customerPojo = restClient.generateCustomer();
        customerToken = restClient.createCustomer(adminToken, customerPojo);

        assertNotNull(customerToken);
        assertEquals(customerToken.firstName, customerPojo.firstName);
        assertEquals(customerToken.lastName, customerPojo.lastName);
        assertEquals(customerToken.login, customerPojo.login);
        assertEquals(customerToken.balance, customerPojo.balance);
    }

    @Test(description = "Delete customer", dependsOnMethods = {"createCustomerTest"})
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Customer feature.")
    public void deleteCustomerTest() {
        restClient.deleteCustomer(adminToken, customerToken.id.toString());
    }

}
