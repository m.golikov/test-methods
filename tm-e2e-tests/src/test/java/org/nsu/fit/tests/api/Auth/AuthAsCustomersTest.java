package org.nsu.fit.tests.api.Auth;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.ContactPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuthAsCustomersTest {
    private AccountTokenPojo adminToken;
    private AccountTokenPojo customerToken;
    private CustomerPojo customer;

    private RestClient restClient;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
    }

    @Test(description = "Authenticate as admin.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Authentication feature.")
    public void authAsAdminTest() {
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Authenticate as customer.", dependsOnMethods = "authAsAdminTest")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Authentication feature.")
    public void authAsCustomerTest() {
        customer = restClient.createCustomer(adminToken, restClient.generateCustomer());
        customerToken = restClient.authenticate(customer.login, customer.pass);
    }

    @Test(description = "Me.", dependsOnMethods = "authAsAdminTest")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Authentication feature.")
    public void getMeTest() {
        ContactPojo contactPojo = restClient.getMe(customerToken);
        assertNotNull(contactPojo);
    }

    @AfterClass
    public void afterClass () {
        restClient.deleteCustomer(adminToken, customer.id.toString());
    }
}
