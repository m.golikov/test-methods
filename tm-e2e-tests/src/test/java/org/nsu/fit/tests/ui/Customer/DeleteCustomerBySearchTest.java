package org.nsu.fit.tests.ui.Customer;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.services.browser.BrowserService;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.tests.ui.screen.AdminScreen;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class DeleteCustomerBySearchTest {

    private Browser browser = null;
    private RestClient restClient;
    private AccountTokenPojo adminToken;
    private CustomerPojo customerPojo;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
        browser = BrowserService.openNewBrowser();
        customerPojo = restClient.generateCustomer();
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Create customer via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Customer feature")
    public void createCustomer() {
        new LoginScreen(browser)
                .loginAsAdmin()
                .createCustomer()
                .fillEmail(customerPojo.login)
                .fillPassword(customerPojo.pass)
                .fillFirstName(customerPojo.firstName)
                .fillLastName(customerPojo.lastName)
                .clickSubmit();


        List<CustomerPojo> customers = restClient.getCustomers(adminToken);
        assertNotNull(customers);
        assertNotEquals(customers.size(), 0);

        List<CustomerPojo> customer = customers.stream()
                .filter(c -> Objects.equals(c.login, customerPojo.login))
                .collect(Collectors.toList());
        assertNotNull(customer);
        assertEquals(customer.size(), 1);
        customerPojo = customer.get(0);
    }

    @Test(description = "Delete customer by search via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Customer feature")
    public void deleteCustomerBySearch() {
        new AdminScreen(browser).searchForCustomer(customerPojo.login).deleteCustomer(customerPojo.login);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<CustomerPojo> customers = restClient.getCustomers(adminToken);
        assertNotNull(customers);
        assertFalse(customers.stream().anyMatch(c -> Objects.equals(c.login, customerPojo.login)));
    }

    @AfterClass
    public void afterClass() {
        if (browser != null) {
            browser.close();
        }
    }
}
