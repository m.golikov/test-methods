package org.nsu.fit.tests.api.Subscription;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.nsu.fit.services.rest.data.SubscriptionPojo;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreateSubscriptionTest {

    private AccountTokenPojo customerToken;
    private SubscriptionPojo subscriptionToken;

    private RestClient restClient;
    private String login;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
        login = "test@mail.ru";
        customerToken = restClient.authenticate(login, "testtest");
    }

    @Test(description = "Create subscription")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Subscription feature.")
    public void createSubscriptionTest() {
        List<PlanPojo> plans = restClient.getAvailablePlans(customerToken);
        assertNotNull(plans);
        assertNotEquals(0, plans.size());

        PlanPojo planPojo = plans.get(0);
        SubscriptionPojo subscriptionPojo = restClient.generateSubscription(planPojo);

        subscriptionToken = restClient.createSubscription(customerToken,subscriptionPojo);
        assertNotNull(subscriptionToken);
        assertEquals(subscriptionToken.planId, subscriptionPojo.planId);
        assertEquals(subscriptionToken.planName, subscriptionPojo.planName);
        assertEquals(subscriptionToken.planDetails, subscriptionPojo.planDetails);
        assertEquals(subscriptionToken.planFee, subscriptionPojo.planFee);
    }

    @Test(description = "Get subscription.", dependsOnMethods = "createSubscriptionTest")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Subscription feature.")
    public void getSubscriptionsTest() {
        AccountTokenPojo adminToken = restClient.authenticate("admin", "setup");

        List<String> subscriptions = restClient.getSubscriptions(adminToken, customerToken.id.toString());
        assertNotNull(subscriptions);
        assertNotEquals(subscriptions.size(), 0);
    }

    @AfterClass
    public void afterClass() {
        restClient.deleteSubscription(customerToken, subscriptionToken.id.toString());
    }
}
