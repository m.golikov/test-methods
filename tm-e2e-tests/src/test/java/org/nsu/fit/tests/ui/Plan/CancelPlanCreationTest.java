package org.nsu.fit.tests.ui.Plan;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.browser.Browser;
import org.nsu.fit.services.browser.BrowserService;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class CancelPlanCreationTest {
    private Browser browser = null;
    private RestClient restClient;
    private AccountTokenPojo adminToken;
    private PlanPojo planPojo;

    @BeforeClass
    public void beforeClass() {
        restClient = new RestClient();
        browser = BrowserService.openNewBrowser();
        planPojo = restClient.generatePlan();
        adminToken = restClient.authenticate("admin", "setup");
    }

    @Test(description = "Cancel plan creation via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Plan feature")
    public void createPlan() {
        new LoginScreen(browser)
                .loginAsAdmin()
                .createPlan()
                .fillName(planPojo.name)
                .fillDetails(planPojo.details)
                .fillFee(planPojo.fee)
                .clickCancel();

        List<PlanPojo> plans = restClient.getPlans(adminToken);
        assertNotNull(plans);
        assertNotEquals(plans.size(), 0);

        List<PlanPojo> plan = plans.stream()
                .filter(c -> Objects.equals(c.name, planPojo.name))
                .collect(Collectors.toList());
        assertNotNull(plan);
        assertEquals(plan.size(), 0);
    }

    @AfterClass
    public void afterClass() {
        if (browser != null) {
            browser.close();
        }
    }
}
